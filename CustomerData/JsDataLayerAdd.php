<?php


namespace Littlelunch\GoogleTagManager\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;

class JsDataLayerAdd implements SectionSourceInterface
{

    public function __construct(){}
    

    /**
     * Get data to ssession array for custumer data
     */
    public function getSectionData() {
        
        $currency_code    = null;
        $product_name     = null;
        $product_id       = null;
        $product_quantity = null;
        $product_price    = null;
        $event            = null;
        if ($_SESSION['LlGtmAddProductAdd'] == 1) {

            $product_id       = $_SESSION['LlGtmAddProductId'];

            $product_name     = $_SESSION['LlGtmAddProductName'];

            $product_quantity = $_SESSION['LlGtmAddProductQty'];

            $product_price    = $_SESSION['LlGtmAddProductPrice'];

            $currency_code    = $_SESSION['LlGtmAddCurrencyCode'];

            $_SESSION['LlGtmAddProductAdd'] = 0;

            $event = 'addToCart';

        }

        return [
            'currencyCode' => $currency_code,
            'products' => [
                    'name'     => $product_name,
                    'id'       => $product_id,
                    'quantity' => $product_quantity,
                    'price'    => $product_price
                ],
            'event'=>$event
        ];

    }

}