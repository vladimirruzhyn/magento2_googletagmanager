<?php


namespace Littlelunch\GoogleTagManager\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;

class JsDataLayerRemove implements SectionSourceInterface
{


    public function __construct(){}

    /**
     * Get data to ssession array for custumer data
     */

    public function getSectionData() {

        $currencyCode = null;
        $product_name = null;
        $product_id = null;
        $product_quantity = null;
        $product_price = null;
        $event = null;
        if (isset($_SESSION['LlGtmRemoveProductDelete']) && $_SESSION['LlGtmRemoveProductDelete'] == 1) {

            $product_id       = $_SESSION['LlGtmRemoveProductId'];
            $product_name     = $_SESSION['LlGtmRemoveProductName'];
            $product_quantity = $_SESSION['LlGtmRemoveProductQty'];
            $product_price    = $_SESSION['LlGtmRemoveProductPrice'];
            $currency_code    = $_SESSION['LlGtmRemoveCurrencyCode'];
            $_SESSION['LlGtmRemoveProductDelete'] = 0;
            $event = 'removeFromCart';

        }

        return [
            'currencyCode' => $currency_code,
            'products' => [
                    'name'     => $product_name,
                    'id'       => $product_id,
                    'quantity' => $product_quantity,
                    'price'    => $product_price
                ],
            'event'=>$event
        ];
    }

}