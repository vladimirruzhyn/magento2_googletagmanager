

define([
    'Magento_Customer/js/customer-data',
    'jquery',
    'underscore'
], function(customerData, $, _){

    'use strict';

    function objectKeyExist(object){
        return _.some(object, function(o) {
            return !_.isEmpty(_.pick(o, ["event", "currencyCode", "products"]));
        })
    }


    function updateDataLayer(_gtmDataLayer, _dataObject, _forceUpdate){
        var event = '',
            currencyCode = '',
            products.name = '',
            products.id = '',
            products.quantity = '',
            products.price = '';

        if(_gtmDataLayer !== undefined && (!objectKeyExist(_gtmDataLayer) || _forceUpdate)){
            if(_.isObject(_dataObject) && _.has(_dataObject, 'event')){
                event = _dataObject.event;
            }

            if(_.isObject(_dataObject) &&_.has(_dataObject, 'currencyCode')){
                currencyCode = _dataObject.currencyCode;
            }

            if(_.isObject(_dataObject) &&_.has(_dataObject, 'products')){
                products = _dataObject.products;
            }

            _gtmDataLayer.push({"event" : event, 
                                "ecommerce" : {
                                    'currencyCode':currencyCode,
                                    'add':{
                                        'products':[{
                                            'name':products.name,
                                            'id':products.id,
                                            'quantity':products.quantity,
                                            'price':products.price
                                        }]
                                    }
                                }});
        }
    }

    return function (options) {
        var dataObject = customerData.get("littlelunch-add-to-cart");

        var gtmDataLayer = window[options.dataLayer];


        dataObject.subscribe(function (_dataObject) {
            updateDataLayer(gtmDataLayer, _dataObject, true);
        }, this);

        if(!_.contains(customerData.getExpiredKeys(), "littlelunch-add-to-cart")){
            updateDataLayer(gtmDataLayer, dataObject(), false)
        }

    }

});