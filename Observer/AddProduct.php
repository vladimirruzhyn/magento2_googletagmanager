<?php
namespace Littlelunch\GoogleTagManager\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Request\Http;
use Magento\Checkout\Model\Session;
use Magento\Catalog\Api\ProductRepositoryInterface;

class AddProduct implements ObserverInterface
{

	protected $_layout;

    protected $_storeManager;

    protected $_request;

    protected $_productRepository;

    protected $_checkoutSession;

	public function __construct(
        Session $checkoutSession,
        StoreManagerInterface $storeManager, 
        ProductRepositoryInterface $productRepository,
        Http $request
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_productRepository = $productRepository;
        $this->_storeManager = $storeManager;
        $this->_request = $request;
	}

    /**
     *
     * Add data to section array for custumer data use
     *
     */

    public function execute(Observer $observer) 
    {	
        $request = $this->_request->getParams();

        $currencyCode = $this->_storeManager
                             ->getStore()
                             ->getCurrentCurrency()
                             ->getCode();
        if (isset($request['product'])) {
        	$product_id = $request['product'];
        } else {
        	$product_id=$this->_checkoutSession->getLastAddedProductId(true);
        }
        $product = $this->_productRepository->getById($product_id);
        $products = array();
        $product_name = $product->getName();
        $product_price = $product->getPrice();
        $product_quantity = 1;
        if (isset($request['qty'])) {
        	$product_quantity = $request['qty'];
        }

        $_SESSION['LlGtmAddProductId']    = $product_id;

        $_SESSION['LlGtmAddProductName']  = $product_name;

        $_SESSION['LlGtmAddProductPrice'] = $product_price;

        $_SESSION['LlGtmAddProductQty']   = $product_quantity;

        $_SESSION['LlGtmAddCurrencyCode'] = $currencyCode;
        
        $_SESSION['LlGtmAddProductAdd']   = '1';

       
    	return $this;
    }
}