<?php
namespace Littlelunch\GoogleTagManager\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Request\Http;
use Magento\Checkout\Model\Cart;

class RemoveProduct implements ObserverInterface
{

    protected $_storeManager;

    protected $_request;

    protected $_cart;

	public function __construct(
        StoreManagerInterface $storeManager, 
        Cart $cart,
        Http $request
    ) {
        $this->_storeManager = $storeManager;
        $this->_request = $request;
        $this->_cart = $cart;  
	}

    /**
     *
     * Add data to section array for custumer data use
     *
     */

    public function execute(Observer $observer) 
    {

        $item_id = $this->_request->getParam('item_id');
        if (is_null($item_id)) {
            $item_id = $this->_request->getParam('id');
        }
        $product = null;
        $item_list = $this->_cart->getQuote()->getItemsCollection();
        foreach ($item_list as $item){
            if ($item->getId() == $item_id) {
                $product = $item;
                break;
            }
        }

        $currencyCode = $this->_storeManager
                             ->getStore()
                             ->getCurrentCurrency()
                             ->getCode();

        $_SESSION['LlGtmRemoveProductId']     = $product->getProductId();

        $_SESSION['LlGtmRemoveProductName']   = $product->getName();

        $_SESSION['LlGtmRemoveProductQty']    = $product->getQty();

        $_SESSION['LlGtmRemoveProductPrice']  = $product->getPrice();

        $_SESSION['LlGtmRemoveProductDelete'] = 1;

        $_SESSION['LlGtmRemoveCurrencyCode']  = $currencyCode;

        return $this;
    }
}