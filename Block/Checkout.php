<?php


namespace Littlelunch\GoogleTagManager\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Checkout\Model\Cart;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\StoreManagerInterface;
use MagePal\GoogleTagManager\Block\GtmCode;

class Checkout extends Template
{


    public function __construct(
        Context $context,
        Cart $cart,
        StoreManagerInterface $storeManager,
        GtmCode $gtmCode,
        $data = []
    )
    {
        $this->_cart = $cart;
        $this->_gtmCode = $gtmCode;
        $this->_storeManager = $storeManager;
        $this->_step = 1;
        /*if (isset($_SESSION['LlGtmCheckoutStep'])) {
            $this->_step = $_SESSION['LlGtmCheckoutStep'];
            $_SESSION['LlGtmCheckoutStep']++;
        } else {
            $_SESSION['LlGtmCheckoutStep'] = 1;
        }*/
        parent::__construct($context, $data);
    }
    
    public function getProduct()
    {
        
        $product_list = array();
        $item_list = $this->_cart->getQuote()->getItemsCollection();
        foreach ($item_list as $item){
            $product = array();
            $product['name'] = $item->getName();
            $product['price'] = $item->getPrice();
            $product['qty'] = $item->getQty();
            $product['id'] = $item->getProductId();
            array_push($product_list, $product);
        }

        $currency_code = $this->_storeManager
                             ->getStore()
                             ->getCurrentCurrency()
                             ->getCode();

        $event = 'checkout';

        return [
            'currencyCode' => $currency_code,
            'product_list' => $product_list,
            'event'=>$event,
            'step'=>$this->_step
        ];
    }

    public function getDataLayerName() {
        return $this->_gtmCode->getDataLayerName();
    }

}
